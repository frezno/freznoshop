# Release Notes for 0.2.x


## Unreleased

### Added
- Starting with a product grid as a first glimpse on the products


## [v0.2.5 (2019-01-02)](https://github.com/frezno/freznoshop/compare/v0.2.4...v0.2.5)

### Added
- Image slider
- Default info if no news are available

### Changed
- Layout somewhat

### Fixed
- Column to row wrapping of the main content
- Typos in changelog-0.2 file


## [v0.2.4 (2018-12-26)](https://github.com/frezno/freznoshop/compare/v0.2.3...v0.2.4)

### Added
- Database seeder for the categories
- News model, migration and seeder
- Displaying News at the home page

### Changed
- Sripts is moved to the end of the file again


## [v0.2.3 (2018-12-23)](https://github.com/frezno/freznoshop/compare/v0.2.2...v0.2.3)

### Added
- Footer
- Routes for footer links
- Static pages of the footer links

### Changed
- Top margin of top navigation


## [v0.2.2 (2018-12-18)](https://github.com/frezno/freznoshop/compare/v0.2.1...v0.2.2)

### Added
- Laravel Telescope
- Top navigation
- Categories model
- Categories migration

### Changed
- Get styles working properly


## v0.2.1 (2018-11-15)

### Restart from scratch, using Laravel 5.7
